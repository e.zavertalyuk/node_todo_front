import { configureStore } from "@reduxjs/toolkit";

import userSlice from "../slices/userSlice";
import todosSlice from "../slices/fetchSlice";
import loginSlice from "../slices/loginSlice";

export const store = configureStore({
   reducer: {
      user: userSlice,
      fetchTodos: todosSlice,
      isLogin: loginSlice,
   }
})