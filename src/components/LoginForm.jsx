import React from 'react'
import { useDispatch } from 'react-redux'
import { useState } from 'react'
import { logIn } from '../slices/loginSlice'
const LoginForm = () => {
   const dispatch = useDispatch()
   const [userName, setUserName] = useState('')
   const handleSubmit = () => {
      dispatch(logIn(userName))
   }
   return (
      <div className='formWrap'>
         <form className='registerForm' onSubmit={() => handleSubmit()}>
            <input
               type='text'
               placeholder='Your name...'
               className='userName'
               value={userName}
               onChange={(e) => setUserName(e.target.value)}
            />
            <button
               type='submit'
               className='registerBtn'
            >
               Register
            </button>
         </form>
      </div>

   )
}

export default LoginForm