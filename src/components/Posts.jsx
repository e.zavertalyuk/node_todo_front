import React from 'react'
import PostItem from './PostItem'
import { useDispatch, useSelector } from 'react-redux'
import { getPosts } from '../slices/fetchSlice'
const Posts = () => {
    const { posts } = useSelector((state) => state.fetchPosts)
    const dispatch = useDispatch()
    return (
        <div>
            <button
                onClick={() => dispatch(getPosts())}
                type='submit'
                className='bg-lime-300  hover:bg-lime-400 transition-all p-2 text-sm'
            >
                Get posts
            </button>
            {posts.map((post) => (
                <PostItem key={post.id}
                    {...post} />

            ))}
        </div>
    )
}

export default Posts
