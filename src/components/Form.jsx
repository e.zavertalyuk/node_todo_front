import React from 'react'

import { useState } from 'react'
import { useDispatch } from 'react-redux';
import { addTodos } from '../slices/fetchSlice';

function formatDate(time) {
    const date = new Date(time);
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear().toString().slice(-2);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}`;
    return formattedDate;
}

const Form = () => {
    const [todoValue, setTodoValue] = useState('')
    const dispatch = useDispatch()
    const addTodoHandler = () => {
        const todo = {
            id: Math.floor(Math.random() * 100) + 1,
            text: todoValue,
            date: formatDate(Date.now()),
            completed: false
        }
        dispatch(addTodos(todo))
        setTodoValue('')
    }
    return (
        <form className='w-full flex' onSubmit={(e) => e.preventDefault()}>
            <input
                type='text'
                placeholder='Type something...'
                value={todoValue}
                onChange={(e) => {
                    setTodoValue(e.target.value)
                }}
                className='w-full p-1 focus:outline-none focus:border-lime-500 focus: border-2 placeholder:text-sm'
            />
            <button
                type='submit'
                onClick={() => addTodoHandler()}
                className='shrink-0 bg-lime-300  hover:bg-lime-400 transition-all px-3 text-sm'
            >
                Submit
            </button>
        </form>
    )
}

export default Form
