import React from 'react'
import { useDispatch } from 'react-redux'
import { logOut } from '../slices/loginSlice'

const Header = () => {
   const dispatch = useDispatch()
   const handleLogOut = () => {
      dispatch(logOut())
   }
   const userName = localStorage.getItem('userName')
   return (
      <>
         <header className='header'>
            <div className='headerWrap'>
               <h2 className='userName'>
                  {`Hello,  ${userName}`}
               </h2>
               <button onClick={() => handleLogOut()} className='logOutBtn'>Log out</button>
            </div>
         </header>
      </>
   )
}
export default Header