import React from 'react'
import { useDispatch } from 'react-redux'
import { delTodo, toggleTodo } from '../slices/fetchSlice'

const TodoItem = ({ todo }) => {
    const dispatch = useDispatch()

    return (

        <div className='flex justify-between items-center my-2'>
            <div onClick={() => dispatch(toggleTodo(todo.id))} className='text-sm px-2 py-2 cursor-pointer bg-lime-300 hover:bg-lime-400'>
                Complete
            </div>
            <div className={` textWrap text-sm ${todo.completed ? 'line-through font-medium text-lime-400' : ''}`}>
                <p className='date'>{todo.date}</p>
                {todo.text}
            </div>
            <div onClick={() => dispatch(delTodo(todo.id))}
                className='text-sm px-2 py-2 flex bg-red-400 hover:bg-red-500 transition-all text-white cursor-pointer'>
                Delete
            </div>
        </div>
    )
}

export default TodoItem
