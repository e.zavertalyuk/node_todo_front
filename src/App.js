import React, { useEffect } from 'react'
import Form from './components/Form'
import TodoItem from './components/TodoItem'
import User from './components/User'
import Footer from './components/Footer'
import { useSelector, useDispatch } from 'react-redux'
import Header from './components/Header'
import LoginForm from './components/LoginForm'
import { getTodos } from './slices/fetchSlice'
function App() {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getTodos())
    }, [dispatch])

    const { isLogined } = useSelector((state) => state.isLogin)
    const { todos } = useSelector((state) => state.fetchTodos)
    return (
        isLogined ?
            <>
                <Header />
                <div className='mainWrap'>

                    <div className='w-1/3'>
                        <h1 className='font-bold my-5'>Redux Toolkit Todo App</h1>
                        <Form />
                        {
                            todos.map((todo) => (
                                <TodoItem
                                    key={todo.id}
                                    todo={todo}
                                />
                            ))
                        }
                    </div>
                </div >

                <Footer />
            </>
            :
            <LoginForm />
    )
}

export default App
