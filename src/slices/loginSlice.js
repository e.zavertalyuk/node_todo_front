import { createSlice } from '@reduxjs/toolkit';
const userName = localStorage.getItem('userName')

const initialState = {
   isLogined: userName ? true : false
}

export const loginSlice = createSlice({
   name: 'login',
   initialState,
   reducers: {
      logIn: (state, action) => {
         state.isLogined = !state.isLogined
         localStorage.setItem('userName', action.payload)
      },
      logOut: (state, action) => {
         state.isLogined = !state.isLogined
         localStorage.removeItem('userName')
         localStorage.removeItem('todos')

      },
   }
})

export const { logIn, logOut } = loginSlice.actions
export default loginSlice.reducer