import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
const initialState = {
   todos: [],
}
export const getTodos = createAsyncThunk(
   'todos/getTodos',
   async (_, { rejectWithValue, dispatch }) => {
      try {
         const response = await fetch('https://node-todo-server.vercel.app/todos')
         const data = await response.json()
         dispatch(setTodos(data))
         return data
      } catch (error) {
         return rejectWithValue(error.message)
      }
   }
)
export const addTodos = createAsyncThunk(
   'todos/addTodo',
   async (data, { rejectWithValue, dispatch }) => {
      try {
         await fetch('https://node-todo-server.vercel.app/todo', {
            method: 'POST',
            headers: {
               'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
         })
         dispatch(addTodo(data))

      } catch (error) {
         return rejectWithValue(error.message)
      }
   }
)
export const toggleTodo = createAsyncThunk(
   'todos/toggleTodo',
   async (id, { rejectWithValue, dispatch }) => {
      try {
         await fetch(`https://node-todo-server.vercel.app/todos/${id}`, {
            method: 'PATCH',
            headers: {
               'Content-Type': 'application/json',
            },
            body: JSON.stringify({ completed: true })
         });

         dispatch(completeTodo(id));
      } catch (error) {
         return rejectWithValue(error.message);
      }
   }
);


export const delTodo = createAsyncThunk(
   'todos/deleteTodo',
   async (id, { rejectWithValue, dispatch }) => {
      try {
         await fetch(`https://node-todo-server.vercel.app/todos/${id}`, {
            method: 'DELETE'
         });
         dispatch(deleteTodo(id))
      } catch (error) {
         return rejectWithValue(error.message);
      }
   }
);



export const todosSlice = createSlice({
   name: 'posts',
   initialState,
   reducers: {
      setTodos: (state, action) => {
         state.todos = action.payload
      },
      addTodo: (state, action) => {
         state.todos.push(action.payload)
      },
      deleteTodo: (state, action) => {
         state.todos = state.todos.filter(todo => todo.id !== action.payload)
      },
      completeTodo: (state, action) => {
         const completedTodo = state.todos.find((todo) => todo.id === action.payload);
         completedTodo.completed = !completedTodo.completed
      }
   },

})

export const { setTodos, deleteTodo, addTodo, completeTodo } = todosSlice.actions
export default todosSlice.reducer